﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public abstract class BGObject
    {
        public int moveObj;
        private int X, Y;
        public Rectangle colRect;
        private Surface mVideo;
        protected Surface BGImage;
        protected Point mPosition;

        public BGObject(Surface v, int x, int y)
        {
            X = x;
            Y = y;
            mVideo = v;
            mPosition = new Point(x, y);
        }

        public void Draw()
        {
            mPosition = new Point(X + moveObj, Y);
            colRect.X = X + moveObj;
            BGImage.Transparent = true;
            BGImage.TransparentColor = Color.Cyan;
            mVideo.Blit(BGImage, mPosition);
        }
    }
}
