﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Level : Sprite
    {
        public int moveObj;
        public double moveBack;
        private Surface mVideo;
        public Surface background;
        public byte[,] tileArray;
        public BGObject[,] objectArray;
        public Pickup[,] itemArray;
        public Wall[,] wallArray;
        public int arrayLength = 140;
        public int arrayHeight = 10;
        public bool einde = false;
        public bool begin = true;
        public bool PAUZE = true;
        public List<Bamboo> bambooList = new List<Bamboo>();
        public SpriteCollection enemyCollection;
        public SpriteCollection itemCollection;
        public Level(Surface v)
        {
            enemyCollection = new SpriteCollection();
            enemyCollection.EnableTickEvent();
            itemCollection = new SpriteCollection();
            itemCollection.EnableTickEvent();
            background = new Surface("BGL1.png").Convert(new Surface(1600, 600));
            mVideo = v;

            tileArray = new byte[,]
            {
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,5,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,5,0,4,0,0,0,0,0,0,0,0,0,5,0,3,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,2,2,2,2,2,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,4,0,0,1,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,1,1,1,1},
                {0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,1,1,1,1,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,5,0,0,0,0,5,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,5,1,1},
                {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,2,2,2,2,0,2,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1},
                {0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,3,0,0,0,0,5,0,0,0,0,1,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1},
                {0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,2,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
                
            };

            objectArray = new BGObject[arrayHeight, arrayLength];
            wallArray = new Wall[arrayHeight, arrayLength];
            itemArray = new Pickup[arrayHeight, arrayLength];
            for (int r = 0; r < arrayHeight; r++)
            {
                for (int k = 0; k < arrayLength; k++)
                {
                    if (tileArray[r, k] == 1)
                    {
                        objectArray[r, k] = new Floor(mVideo, (k * 50) + moveObj, (r * 65));
                        objectArray[r, k].colRect = new Rectangle((k * 50) + moveObj, (r * 65), 54, 54);
                    }
                    if (tileArray[r, k] == 2)
                    {
                        objectArray[r, k] = new Platform(mVideo, (k * 50) + moveObj, (r * 65));
                        objectArray[r, k].colRect = new Rectangle((k * 50) + moveObj, (r * 65), 101, 50);
                    }
                    if (tileArray[r, k] == 3)
                    {
                        bambooList.Add(new Bamboo(mVideo, (k * 50) + moveObj, (r * 65) + moveObj,false));
                    }
                    if (tileArray[r, k] == 4)
                    {
                        objectArray[r, k] = new Floor(mVideo, (k * 50) + moveObj, (r * 65));
                        objectArray[r, k].colRect = new Rectangle((k * 50) + moveObj, (r * 65), 54, 54);
                        wallArray[r, k] = new Wall(mVideo, (k * 50) + moveObj, (r * 65) - 324);
                        wallArray[r, k].wallRect = new Rectangle((k * 50) + moveObj, (r * 65) - 324, 54, 378);
                    }
                    if (tileArray[r, k] == 5)
                    {
                        bambooList.Add(new Bamboo(mVideo, (k * 50) + moveObj, (r * 65) + moveObj, true));
                    }
                    if (tileArray[r, k] == 6)
                    {
                        itemArray[r, k] = new Girlfriend(mVideo, (k * 50) + moveObj, (r * 65));
                        itemArray[r, k].oRect = new Rectangle((k * 50) + moveObj, (r * 65), 126, 149);
                        itemCollection.Add(itemArray[r, k]); 
                    }

                }
            }
            foreach (Bamboo b in bambooList)
            {
                if (b != null)
                {
                    enemyCollection.Add(b);
                }
            }
        }

        public void Draw()
        {
            mVideo.Blit(background, new Point(0, 0), new Rectangle(0 + (int)moveBack, 0, 1000, 600));
            for (int r = 0; r < arrayHeight; r++)
            {
                for (int k = 0; k < arrayLength; k++)
                {
                    if (objectArray[r, k] != null)
                    {
                        objectArray[r, k].moveObj = moveObj;
                        objectArray[r, k].Draw();
                    }
                    if (wallArray[r, k] != null)
                    {
                        wallArray[r, k].moveObj = moveObj;
                        wallArray[r, k].Draw();
                    }
                    if (itemArray[r, k] != null)
                    {
                        itemArray[r, k].moveObj = moveObj;
                        itemArray[r, k].Draw();
                        
                    }
                }
            }
            foreach (Bamboo b in bambooList)
            {
                if (b != null)
                {
                    if (PAUZE == false)
                    {
                        b.PAUZE = false;
                    }
                    b.bambooDraw();
                }
            }

            if (objectArray[arrayHeight - 1, arrayLength - 1].colRect.X <= 1000)
            {
                einde = true;
            }
            else einde = false;
            if (objectArray[9, 0].colRect.X >= 0)
            {
                begin = true;
            }
            else begin = false;
        }

        public void bambooDown()
        {
            foreach (Bamboo b in bambooList)
            {
                b.bambooDrop(b.drop);
            }
        }
    }
}
