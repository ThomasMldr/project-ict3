﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Home : Sprite
    {
        private Surface mVideo;
        private Surface background;
        private Surface zin1;
        private Surface zin2;
        private Surface zin3;
        private Surface zin4;
        private Surface zin5;
        private SdlDotNet.Graphics.Font font1;
        private SdlDotNet.Graphics.Font font2;
        private SdlDotNet.Graphics.Font font3;
        private SdlDotNet.Graphics.Font font4;
        private SdlDotNet.Graphics.Font font5;
        public bool Show = true;
        public bool start = true;
        public bool gameover = false;
        public bool win = false;
        private int blink;

        public Home(Surface v)
        {

            mVideo = v;
            Events.KeyboardDown += Events_KeyboardDown;
            background = new Surface("homeBack.jpg").Convert(mVideo);
            Events.Quit += Events_Quit;
            Events.Tick += Events_Tick;

            startscreen();
            
        }

        public void startscreen()
        {
            font1 = new SdlDotNet.Graphics.Font("Harakiri.ttf", 150);
            zin1 = font1.Render("Save your species!", Color.DarkRed);
            font2 = new SdlDotNet.Graphics.Font("PinkPanda.ttf", 40);
            zin2 = font2.Render("Why don't pandas have intercourse?!", Color.DarkRed);
            font3 = new SdlDotNet.Graphics.Font("Shanghai.ttf", 50);
            font4 = new SdlDotNet.Graphics.Font("GangOfThree.ttf", 40);
            zin4 = font4.Render("Go rescue your girlfriend from the evil bamboo!", Color.DarkRed);
            zin3 = font3.Render("Go have some intercourse like a hero!", Color.DarkRed);
            font5 = new SdlDotNet.Graphics.Font("Zenzai Itacha.ttf", 50);
            zin5 = font5.Render("Press   S P A C  E    to start playing!", Color.DarkRed);
        }

        public void winscreen()
        {
            font1 = new SdlDotNet.Graphics.Font("Harakiri.ttf", 150);
            zin1 = font1.Render("Nice going, playpanda!", Color.DeepPink);
            font2 = new SdlDotNet.Graphics.Font("PinkPanda.ttf", 40);
            zin2 = font2.Render("You totally hooked up with that girl!!", Color.DeepPink);
            font3 = new SdlDotNet.Graphics.Font("Shanghai.ttf", 50);
            font4 = new SdlDotNet.Graphics.Font("GangOfThree.ttf", 40);
            zin4 = font4.Render("Don't celebrate just yet!", Color.DeepPink);
            zin3 = font3.Render("On to level 2!!!!", Color.DeepPink);
            font5 = new SdlDotNet.Graphics.Font("Zenzai Itacha.ttf", 50);
            zin5 = font5.Render("Press   S P A C  E    to start playing!", Color.DeepPink);
        }

        void Events_Tick(object sender, TickEventArgs e)
        {
            Draw();

        }

        void Events_Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        void Events_KeyboardDown(object sender, KeyboardEventArgs e)
        {
            KeyboardState keys = new KeyboardState();
            if (keys.IsKeyPressed(Key.Space))
            {
                Show = false;
            }
        }

        public override void Update(TickEventArgs args)
        {
            Draw();
        }

        public void Draw()
        {
            if(Show == true)
            {
                mVideo.Blit(background, new Point(0, 0), new Rectangle(0, 0, 1000, 600));
                
                try
                {
                    mVideo.Blit(zin1, new Point(0,0));
                }
                catch (NullReferenceException e)
                {
                    Console.WriteLine(zin1.ToString());
                    Console.WriteLine(e.Message);
                }
                mVideo.Blit(zin2, new Point(0,150));
                mVideo.Blit(zin3, new Point(0,210));
                mVideo.Blit(zin4, new Point(0,280));
                blink++;
                if (blink < 40)
                {
                    mVideo.Blit(zin5, new Point(0,320));
                }
                if (blink >= 60)
                {
                    blink = 0;
                }
            }
        }
    }
}
