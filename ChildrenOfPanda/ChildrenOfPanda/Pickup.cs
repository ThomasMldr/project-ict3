﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public abstract class Pickup : Sprite
    {
        public int travel;
        public int oX, oY;
        public Rectangle oRect;
        public Rectangle gRect;
        private Surface mVideo;
        protected Surface oImage;
        protected Point mPosition;
        public int moveObj;
        private int slower;

        public Pickup(Surface v, int x, int y)
        {
            oX = x;
            oY = y;
            mVideo = v;
            mPosition = new Point(oX, oY);
        }
        public override void Update(TickEventArgs args)
        {
           oImage.Transparent = true;
            oImage.TransparentColor = Color.Cyan;
        }



        public virtual void Draw()
        {
            oX += moveObj;
            mPosition = new Point(oX, oY);
            oRect.X += moveObj;
            oImage.Transparent = true;
            oImage.TransparentColor = Color.Cyan;
            mVideo.Blit(oImage, mPosition);


        }
    }
}
