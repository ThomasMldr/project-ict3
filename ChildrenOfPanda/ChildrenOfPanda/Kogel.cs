﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Kogel : Sprite
    {
        public int travel;
        public int kX, kY;
        public bool kDir;
        public Rectangle kRect;
        public Rectangle lRect;
        private Surface mVideo;
        protected Surface kImage;
        protected Point mPosition;

        public Kogel(Surface v, int x, int y, bool dir)
        {
            X = x;
            Y = y;
            kDir = dir;
            mVideo = v;
            mPosition = new Point(X, Y);
        }
        public override void Update(TickEventArgs args)
        {

        }

        

        public virtual void Draw()
        {
            if (kDir == false)
            {
                travel = 3;
            }
            else
            {
                travel = -3;
            }
            X += travel;
            mPosition = new Point(X, Y);
            kRect.X = X + travel;
            kImage.Transparent = true;
            kImage.TransparentColor = Color.Cyan;
            mVideo.Blit(kImage, mPosition);
            
            
        }
    }
}
