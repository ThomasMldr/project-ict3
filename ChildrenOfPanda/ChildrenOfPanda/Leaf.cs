﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Leaf : Kogel
    {
        private int lX, lY;
        private int xRect;
        private int slower;
        public bool lDir { get; private set; }
        private Surface mVideo;

        public Leaf(Surface v, int x, int y, bool dir)
            : base(v, x, y, dir)
        {
            lX = x;
            lY = y;
            xRect = 0;
            lDir = dir;
            mVideo = v;

            if (lDir)
            {
                travel = -3;
            }
            else if(lDir == false)
            {
                travel = 3;
            }
            kImage = new Surface("leafRight.png");
            kImage.Transparent = true;
            kImage.TransparentColor = Color.Cyan;
            kRect = new Rectangle(x, y, 16, 23);
        }

        public override void Update(TickEventArgs args)
        {
           if (lDir)
           {
                slower++;
                if (slower >= 2)
                {
                   xRect -= 16;
                   if (xRect < 0)
                   {
                       xRect = 240;
                   }
                    slower = 0;
                }
            }
            else
            {
               slower++;
                if (slower >= 2)
                {
                    xRect += 16;
                    if (xRect >= 256)
                    {
                        xRect = 0;
                    }
                    slower = 0;
                }
            }
           kImage.Transparent = true;
           kImage.TransparentColor = Color.Cyan;
        }

        public override void Draw()
        {
            lRect = new Rectangle(xRect, 0, 16, 23);
            if (kDir == false)
            {
                travel = 3;
            }
            else
            {
                travel = -3;
            }
            X += travel;
            kRect.X += travel;
            mVideo.Blit(kImage, new Point(X, Y), lRect);
        }
    }
}
