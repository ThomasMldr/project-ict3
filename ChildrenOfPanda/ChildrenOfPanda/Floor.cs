﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;

namespace ChildrenOfPanda
{
    public class Floor : BGObject
    {
        public Floor(Surface v, int x, int y)
            :base(v, x , y)
        {
            BGImage = new Surface("Platform4.png").CreateScaledSurface(0.5);
            colRect = new Rectangle(x, y, 54, 3);
        }
    }
}
