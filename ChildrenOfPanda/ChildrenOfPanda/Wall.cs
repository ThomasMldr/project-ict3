﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Wall
    {
        public int moveObj;
        private int X, Y;
        private Surface mVideo;
        protected Surface BGImage;
        protected Point mPosition;
        public Rectangle wallRect;
        public Rectangle topRect;

        public Wall(Surface v, int x, int y)
        {
            X = x;
            Y = y;
            mVideo = v;
            mPosition = new Point(x, y);
            BGImage = new Surface("Platform5.png").CreateScaledSurface(0.5);
            wallRect = new Rectangle(X,Y-324,54,378);
            topRect = new Rectangle(X,Y,54,10);
        }
        public void Draw()
        {
            mPosition = new Point(X + moveObj, Y);
            wallRect.X = X + moveObj;
            topRect.X = X + moveObj;
            mVideo.Blit(BGImage, mPosition);
        }
    }
}
