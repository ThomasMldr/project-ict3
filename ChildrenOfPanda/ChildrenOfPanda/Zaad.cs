﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Zaad : Kogel
    {
        private int zX, zY;
        private bool zDir;
        public Zaad(Surface v, int x, int y, bool dir)
            : base(v, x, y, dir)
        {
            zX = x;
            zY = y;
            zDir = dir;
            if (dir)
            {
                kImage = new Surface("fireLeft.png");
            }
            else
            {
                kImage = new Surface("fireRight.png");
            }
            kImage.Transparent = true;
            kImage.TransparentColor = Color.Cyan;
            kRect = new Rectangle(x, y, 25, 14);
        }

        public override void Update(TickEventArgs args)
        {
            kRect = new Rectangle(zX, zY, 25, 14);
        }
    }
}
