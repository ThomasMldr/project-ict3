﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Bamboo : Sprite
    {
        public int distance, check, distCheck;
        public bool left = true;
        public bool right = false;
        public int lives = 5;
        public bool dead = false;
        public bool drop = true;
        public bool PAUZE = true;
        private Surface mVideo;
        private Surface enemy;
        public SpriteCollection zaadCollection = new SpriteCollection();
        private Rectangle bRect;
        public Rectangle bodyRect;
        public Rectangle shootRect;  
        public Rectangle jumpRect;
        public int bX, bY, vorige;
        public int speed = 2;
        private long lastShot = DateTime.Now.Ticks;
        private int minInterval = 9999999;
        private int bXRect;
        private int slower;
        private int test = 0;

        public Bamboo(Surface v, int x, int y, bool kort)
        {
            mVideo = v;
            enemy = new Surface("bamboo.png").Convert(Video.Screen, true, true);
            enemy.Transparent = true;
            enemy.TransparentColor = Color.Cyan;
            bY = y - 100;
            bX = x;
            Random rand = new Random((int)DateTime.Now.Ticks);
            distCheck = rand.Next(5,15);
            Console.WriteLine(distCheck);
            if (distCheck%2 == 0)
            {
                right = true;
                left = false;
            }
            else
            {
                right = false;
                left = true;
            }
            drop = true;
            if (kort)
            {
                distCheck = 5;
            }
        }

        public override void Update(TickEventArgs args)
        {
            if (PAUZE == false)
            {
                bodyRect = new Rectangle((bX + 35), (bY + 80), 35, 24);
                shootRect = new Rectangle((bX + 35), bY, 35, 104);
                if (distance >= distCheck)
                {
                    distance = 0;
                    if (left)
                    {
                        left = false;
                        right = true;
                    }
                    else if (right)
                    {
                        right = false;
                        left = true;
                    }
                }
               
                if (left && right == false)
                {
                    slower++;
                    right = false;
                    if (slower >= 9)
                    {
                        slower = 0;
                        bXRect -= 104;
                        if (bXRect < 0)
                        {
                            bXRect = enemy.Width - 104;
                        }
                    }
                    bX -= speed;
                    bodyRect.X -= speed;
                    shootRect.X -= speed;
                }
                if (right && left == false)
                {
                    slower++;
                    left = false;
                    if (slower >= 9)
                    {
                        slower = 0;
                        bXRect += 104;
                        if (bXRect >= enemy.Width)
                        {
                            bXRect = 0;
                        }
                    }
                    bX += speed;
                    bodyRect.X += speed;
                    shootRect.X += speed;
                }
                Random rand2 = new Random();
                if ((int)(args.Tick*rand2.NextDouble()*distCheck)%103 == 0)//min of meer random interval die genoeg uit elkaar ligt om kogels te schieten
                {
                    if (bX < 1600 && bX> 0)
                    {
                        if (DateTime.Now.Ticks - lastShot > minInterval)
                        {
                            Shoot();
                        }
                    }
                }
                bambooDraw();
                bambooDrop(drop);
            }

        }

        public void bambooDrop(bool valt)
        {
            if (valt)
            {
                vorige = bY;
                bY += 5;
                bodyRect.Y += 5;
            }
            else
            {
                bY = vorige;
            }
        }

        public void bambooDraw()
        {
            bRect = new Rectangle(bXRect, 0, 104,104);
            mVideo.Blit(enemy, new Point(bX, bY), bRect);
            foreach (Zaad z in zaadCollection)
            {
                z.Draw();
            }
        }

        public void Shoot()
        {
            if (dead == false)
            {
                test++;
                Console.WriteLine("shoot" + distCheck + "  " + test);
                lastShot = DateTime.Now.Ticks;
                zaadCollection.Add(new Zaad(mVideo, bX, bY+18, left));
            }
            

        }

        public void Hit()
        {
            lives--;
            Console.WriteLine("bamboogeraakt " + lives);
            if(lives <= 0)
            {
                dead = true;
                Console.WriteLine("bamboodoooood");
            }
        }
    }
}
