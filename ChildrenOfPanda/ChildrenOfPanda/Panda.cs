﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;


namespace ChildrenOfPanda
{
    public class Panda : Sprite
    {
        public bool left, right, lastDir, dive;
        public bool leftMotion, rightMotion;
        public bool spring;
        public bool canJump;
        public bool einde = false;
        public bool begin = true;
        public bool bots = false;
        public bool PAUZE = true;
        public bool hit;
        public bool hitWallRight = false;
        public bool hitWallLeft = false;
        public int hitRecup;
        private int hitFlash;
        public int lives = 5;
        private int xRect;
        private int hpYRect;
        private int lig;
        private int feet = 0;
        private int nose = 0;
        private int buk = 0;
        public int speed = 8;
        public int pX { get; set; }
        public int pY { get; set; }
        private int time;
        public int vorige { get; private set; }
        private Rectangle pRect, hpRect;
        public Rectangle hitRect, platfRect, shootRect, bodyRect, noseRect;
        public SpriteCollection leafCollection;
        private Surface mVideo;
        private Surface walkLeft;
        private Surface walkRight;
        private Surface jumpLeft;
        private Surface jumpRight;
        private Surface diveLeft;
        private Surface diveRight;
        private Surface hpBar;
        private int slower = 0;

        public Panda(Surface v)
        {
            mVideo = v;
            walkLeft = new Surface("pandaLeft.png").Convert(Video.Screen, true, true);
            walkLeft.Transparent = true;
            walkLeft.TransparentColor = Color.Cyan;
            walkRight = new Surface("pandaRight.png").Convert(Video.Screen, true, true);
            walkRight.Transparent = true;
            walkRight.TransparentColor = Color.Cyan;
            jumpLeft = new Surface("jumpLeft.png").Convert(Video.Screen, true, true);
            jumpLeft.Transparent = true;
            jumpLeft.TransparentColor = Color.Cyan;
            jumpRight = new Surface("jumpRight.png").Convert(Video.Screen, true, true);
            jumpRight.Transparent = true;
            jumpRight.TransparentColor = Color.Cyan;
            diveLeft = new Surface("diveLeft.png").Convert(Video.Screen, true, true);
            diveLeft.Transparent = true;
            diveLeft.TransparentColor = Color.Cyan;
            diveRight = new Surface("diveRight.png").Convert(Video.Screen, true, true);
            diveRight.Transparent = true;
            diveRight.TransparentColor = Color.Cyan;
            hpBar = new Surface("hpBar.png").Convert(Video.Screen, true, true);
            hpBar.Transparent = true;
            hpBar.TransparentColor = Color.Cyan;
            leafCollection = new SpriteCollection();
            leafCollection.EnableTickEvent();
            pY = 600;
            left = right = spring = false;
        }

        public override void Update(TickEventArgs args)
        {
            if (PAUZE == false)
            {
                if (right && left == false)
                {
                    lastDir = false;
                    hitWallLeft = false;
                    feet = 0; //rectangle moet onder de voeten hangen, niet onder het hoofd
                    nose = 100;
                    slower++; //sprite mag niet te snel tussen twee afbeeldingen springen
                    begin = false;//zodat we niet links uit het scherm kunnen lopen
                    if (slower >= 4)//vertraging op blitten van afbeelding tijdens lopen
                    {
                        slower = 0;
                        xRect -= 130;

                        if (xRect < 0)
                        {
                            xRect = 260;
                        }
                    }
                    if (hitWallRight == false)
                    {
                        if (pX < 750 - (walkLeft.Width / 3) || einde)
                        {
                            if (pX < mVideo.Width - walkLeft.Width / 3)
                            {
                                pX += speed; //verplaatsen naar rechts
                                hitWallRight = false;
                            }
                        }
                        else
                        {
                            rightMotion = true;
                        } 
                    }
                    //else
                    //{
                    //    rightMotion = false;
                    //}
                }
                if (left && right == false)
                {
                    lastDir = true;
                    hitWallRight = false;
                    feet = 30;//rectangle moet onder de voeten hangen, niet onder het hoofd
                    nose = 0;//neus staat links
                    slower++;//sprite mag niet te snel tussen afbeeldingen springen, ziet er zo iets natuurlijker uit
                    einde = false;//zodat we niet rechts uit het scherm kunnen lopen
                    if (slower >= 4)//vertraging op blitten van afbeelding tijdens lopen
                    {
                        slower = 0;
                        xRect += 130;

                        if (xRect >= 390)
                        {
                            xRect = 0;
                        }
                    }

                    if (hitWallLeft == false)
                    {
                        if (pX > 250 || begin)
                        {
                            if (pX > 0)
                            {
                                pX -= speed;
                                hitWallLeft = false;
                            }
                        }
                        else
                        {
                            leftMotion = true;
                        } 
                    }
                    //else
                    //{
                    //    leftMotion = false;
                    //}
                }
                switch (lives)
                {
                    case 5:
                        {
                            hpYRect = 0;
                        }
                        break;
                    case 4:
                        {
                            hpYRect = 75;
                        }
                        break;
                    case 3:
                        {
                            hpYRect = 150;
                        }
                        break;
                    case 2:
                        {
                            hpYRect = 225;
                        }
                        break;
                    case 1:
                        {
                            hpYRect = 300;
                        }
                        break;
                    case 0:
                        {
                            Kill();
                            Dispose(true);
                        }
                        break;
                    default:
                        {
                            lives = 5;
                        }
                        break;
                }

                #region springfunctie
                if (spring)
                {
                    vorige = pY;
                    pY = (int)Jump(time, pY, false);
                    time++;
                    if (Jump(time, pY, false) > vorige)//als we aan het vallen zijn
                    {
                        Gravity(bots = true);
                        canJump = false;
                        spring = false;
                        if (bots)//en als we iets raken
                        {
                            spring = false;//sprong gedaan
                            time = 0;
                            Gravity(bots = true);
                        }
                    }
                    else // tijdens stijgen
                    {
                        Gravity(bots = false);//mag niet blijven hangen als hij omhoog springt!
                        canJump = false;
                    }
                }
                #endregion

                if (dive)
                {
                    leftMotion = false;
                    rightMotion = false;
                    pX = lig;
                    buk = 50;
                }

                hitRect = new Rectangle(pX + feet, pY + 100, 90, 4);
                shootRect = new Rectangle(pX + nose, pY + buk, 30, 104 - buk);
                platfRect = new Rectangle(pX + feet, pY + 96, 90, 1);//om te controleren of hij niet halfweg een platform staat
                bodyRect = new Rectangle(pX, pY, 130, 104);
                noseRect = new Rectangle(pX+nose, pY+30, 30,59);
                Draw();
                buk = 0;
            }

        }//end of update tick

        public override void Update(KeyboardEventArgs args)
        {
            KeyboardState keys = new KeyboardState();

            if (keys.IsKeyPressed(Key.LeftArrow))
            {
                left = true;
                lastDir = true;
                if (hitWallRight)
                {
                    //pX -= 10;
                    hitWallRight = false;
                    hitWallLeft = false;
                }
            }
            else
            {
                left = false;
            }

            if (keys.IsKeyPressed(Key.RightArrow))
            {
                right = true;
                lastDir = false;
                if (hitWallLeft)
                {
                    //pX += 10;
                    hitWallLeft = false;
                    hitWallRight = false;
                }
            }
            else
            {
                right = false;
            }

            if (keys.IsKeyPressed(Key.UpArrow))
            {
                if (canJump == true)
                {
                    spring = true;
                }
            }
            if (keys.IsKeyPressed(Key.DownArrow))
            {
                dive = true;
                buk = 50;
                lig = pX;
            }
            else
            {
                if (keys.IsKeyPressed(Key.Space))
                {
                    Shoot();
                }
                dive = false;
                buk = 0;
            }
        }//end of update keyboard

        public static double Jump(int tijd, int lastYPos, bool highJump)
        {
            double value = (highJump == true) ? 2.4 : 1.9;
            int result = -((int)((value * Math.Sin(0.9) * tijd) - (.5 * 0.1 * (tijd * tijd)))) + lastYPos;

            return result;
        }

        public void Gravity(bool allowed) //blijven vallen, tenzij hij iets raakt.
        {
            if (allowed == false && spring == false)//als we niets raken en niet aan het springen zijn
            {
                vorige = pY;
                pY += 15;
            }
            else
            {
                if (vorige != 0 && spring == false)
                {
                    pY = vorige;
                    canJump = false;
                }
            }

        }
        public void Shoot()
        {
            leafCollection.Add(new Leaf(mVideo, pX + nose, pY + 50, lastDir));
        }
        public void Draw()
        {
            if (hitFlash <= 5)
            {
                pRect = new Rectangle(xRect, 0, 130, 104);
                if (lastDir == true && spring == false && dive == false)
                {
                    mVideo.Blit(walkLeft, new Point(pX, pY), pRect);
                }
                else if (lastDir == false && spring == false && dive == false)
                {
                    mVideo.Blit(walkRight, new Point(pX, pY), pRect);
                }
                else if (lastDir == true && spring == true)
                {
                    mVideo.Blit(jumpLeft, new Point(pX, pY), new Rectangle(0, 0, 128, 104));
                }
                else if (lastDir == false && spring == true)
                {
                    mVideo.Blit(jumpRight, new Point(pX, pY), new Rectangle(0, 0, 128, 104));
                }
                else if (lastDir == true && dive == true)
                {
                    mVideo.Blit(diveLeft, new Point(pX, pY), new Rectangle(0, 0, 130, 104));
                }
                else if (lastDir == false && dive == true)
                {
                    mVideo.Blit(diveRight, new Point(pX, pY), new Rectangle(0, 0, 130, 104));
                }
                foreach (Leaf l in leafCollection)
                {
                    l.Draw();
                }
            }
            if (hit == true)
            {
                hitFlash++;
                hitRecup++;
                if (hitFlash >= 10)
                {
                    hitFlash = 0;
                }
            }
            if (hitRecup > 180)
            {
                hitRecup = 0;
                hitFlash = 0;
                hit = false;
            }

            hpRect = new Rectangle(0, hpYRect, 322, 75);
            mVideo.Blit(hpBar, new Point(0, 0), hpRect);

        }//tekent de sprites van de panda (lopen en springen)
    }
}
