﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Manager
    {
        private Surface mVideo;
        private Panda p;
        private Level l;
        private Home h;
        private int hoogte, langhoogte;
        private SpriteCollection collection;
        public Surface startsc = new Surface("homeBack.jpg");
        
        public Manager()
        {
            Events.KeyboardDown +=Events_KeyboardDown;
            Events.KeyboardUp +=Events_KeyboardUp;
            Events.Tick += Events_Tick;
            Events.Quit += Events_Quit;
            //Events.Fps = 3;
            mVideo = Video.SetVideoMode(1000, 600);
            mVideo.Blit(startsc, new Point(0, 0), new Rectangle(0, 0, 1000, 600));
            collection = new SpriteCollection();
            h = new Home(mVideo);
            p = new Panda(mVideo);
            l = new Level(mVideo);
            
            collection.Add(h);
            collection.Add(p);
            collection.Add(l);
            collection.EnableTickEvent();
            collection.EnableKeyboardEvent();
            Events.Run();
        }

        void Events_Tick(object sender, TickEventArgs e)
        {
            if(h.Show == true)
            {
                h.Draw();
                //mVideo.Blit(startsc, new Point(0, 0), new Rectangle(0, 0, 1000, 600));
                l.PAUZE = true;
                p.PAUZE = true;
                h.Show = true;
               
            }
            else
            {
                l.PAUZE = false;
                p.PAUZE = false;
                h.Show = false;
                if (p.leftMotion == true)
                {
                    l.einde = false;
                }
                if (p.rightMotion == true)
                {
                    l.begin = false;

                }
                Motion(p.leftMotion, p.rightMotion);
                
                l.Draw();
                
                ColDet();
                p.Gravity(p.bots);
                p.Draw();
                p.leftMotion = p.rightMotion = false;
            }
            mVideo.Update();
        }

        void Events_Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Events_KeyboardDown(object sender, KeyboardEventArgs e)
        {
            KeyboardState keys = new KeyboardState();
            if (keys.IsKeyPressed(Key.Escape))
            {
                Events.QuitApplication();
            }
            Motion(p.leftMotion, p.rightMotion);
        }

        private void Events_KeyboardUp(object sender, KeyboardEventArgs e)
        {
            
        }

        public void ColDet()
        {
            for (int r = 0; r < l.arrayHeight; r++)
            {
                for (int k = 0; k < l.arrayLength; k++)
                {
                    if (l.objectArray[r, k] != null)
                    {
                        if (l.objectArray[r, k].colRect.IntersectsWith(p.hitRect))//we raken iets
                        {
                            p.bots = true;
                            p.Gravity(p.bots = true);//de sprong moet afgebroken worden want iets geraakt
                            p.bots = true;// er moet gestopt worden met vallen
                            if (l.objectArray[r, k].colRect.IntersectsWith(p.platfRect) &&p.spring == false)//zo blijft hij niet halfweg een platform staan
                            {//spring moet false zijn, want tijdens omhoog springen mag dit geen fouten opleveren
                                p.pY -= 6;
                            }
                        }
                        else if (l.wallArray[r, k] != null)
                        {
                            if (l.wallArray[r, k].topRect.IntersectsWith(p.hitRect))
                            {
                                p.bots = true;
                                p .Gravity(p.bots = true);//de sprong moet afgebroken worden want iets geraakt
                                p.bots = true;// er moet gestopt worden met vallen
                                if (l.wallArray[r, k].topRect.IntersectsWith(p.platfRect) && p.spring == false)//zo blijft hij niet halfweg een platform staan
                                {//spring moet false zijn, want tijdens omhoog springen mag dit geen fouten opleveren
                                    p.pY -= 6;
                                }
                            }
                        }
                        else//we raken niets
                        {
                            p.bots = false; //eventjes aan de panda laten weten dat hij niets raakt en dus moet vallen
                            p.canJump = false;
                            
                        }
                        foreach (Girlfriend g in l.itemCollection)
                        {
                            if (p.bodyRect.IntersectsWith(g.oRect))
                            {
                                h.Show = true;
                                h.winscreen();
                                p.pX = 10;
                                p.pY = 10;
                                l.enemyCollection.Clear();
                                l.bambooList.Clear();
                                l.Dispose();
                                //l.itemCollection.Clear();
                                l = new Level2(mVideo);
                                reload();
                                ColDet();
                            }
                        }

                        foreach (Bamboo b in l.bambooList)
                        {
                            if (l.objectArray[r, k].colRect.IntersectsWith(b.bodyRect))
                            {
                                if (b.check != k)
                                {
                                    b.distance++;
                                }
                                b.check = k;
                                b.drop = false;
                                b.speed = 2;
                                b.bY = b.bodyRect.Y = b.vorige;
                                b.bX = b.bodyRect.X - 35;
                            }
                            else
                            {
                                if (b.drop == false)
                                {
                                    b.drop = true;
                                }
                            }
                            if(l.objectArray[l.arrayHeight-1,0].colRect.X >= b.bX)
                            {
                                b.right = true;
                                b.left = false;
                            }
                            if (l.wallArray[r, k] != null)
                            {
                                if (b.bodyRect.IntersectsWith(l.wallArray[r, k].wallRect))
                                {
                                    if (b.right == false)
                                    {
                                        b.bX += 3;
                                        b.left = false;
                                        b.right = true;
                                    }
                                    else
                                    {
                                        b.right = false;
                                        b.left = true;
                                    }
                                } 
                            }

                            if (b.bodyRect.IntersectsWith(p.hitRect))
                            {
                                if (p.hit == false)
                                {
                                    Console.WriteLine("auw");
                                    p.hit = true;
                                    p.lives--;
                                }
                            }
                            
                            foreach (Zaad z in b.zaadCollection)
                            {
                                if (z.kRect.IntersectsWith(p.shootRect))
                                {
                                    if (p.hit == false)
                                    {
                                        Console.WriteLine("geraakt");
                                        p.lives--;
                                        p.hit = true;
                                    }
                                    z.Kill();
                                    z.Dispose();
                                    b.zaadCollection.Remove(z);
                                    break;
                                    
                                }
                                else if (z.X < 0 || z.X > 1000)
                                {
                                    z.Kill();
                                    z.Dispose();
                                    b.zaadCollection.Remove(z);
                                    break;
                                }
                            }
                            foreach (Leaf g in p.leafCollection)
                            {
                                if (g.kRect.IntersectsWith(b.shootRect))
                                {
                                    b.Hit();
                                    g.Kill();
                                    g.Dispose();
                                    p.leafCollection.Remove(g);
                                    break;
                                }
                                else if (g.X < 0 || g.X > 1000)
                                {
                                    g.Kill();
                                    g.Dispose();
                                    p.leafCollection.Remove(g);
                                    break;
                                }
                            }
                        }
                        foreach (Bamboo b in l.bambooList)
                        {
                            if (b.dead == true) 
                            { 
                                b.Kill();
                                b.Dispose();
                                l.bambooList.Remove(b);
                                if (l.bambooList.Count == 0)
                                {
                                    l.bambooList.Clear();
                                }
                                break;
                            }
                        }
                    }
                    if (l.wallArray[r, k] != null)
                    {
                        if (l.wallArray[r,k].wallRect.IntersectsWith(p.noseRect))
                        {
                            if (p.lastDir == true)
                            {
                                p.hitWallLeft = true;
                                p.pX += 3;
                            }
                            else
                            {
                                p.hitWallRight = true;
                                p.pX -= 3;
                            }
                        }
                    }
                }
            }
            //ook even kijken of hij op hetzelfde niveau staat. Zodat hij zeker niet kan springen terwijl hij aan het vallen is.
            if (p.pY == langhoogte)
            {
                p.canJump = true;
            }
            langhoogte = hoogte;
            hoogte = p.pY;
            if (p.pY > 496)// als we uit scherm vallen, mag zoiso niet
            {
                p.spring = false;//mag niet springen
                p.Gravity(false);
                p.pY = 496;
            }
        }//einde coldet

        public void reload()
        {
           
        }

        public void Motion(bool links, bool rechts)
        {
            p.einde = l.einde;
            p.begin = l.begin;
            if (l.einde == false && l.begin == false)
            {
                if (links == true)
                {
                    l.moveObj += p.speed;
                    l.moveBack -= 0.8;
                    foreach (Bamboo b in l.enemyCollection)
                    {
                        if (b.left == true)
                        {
                            b.bX += p.speed - b.speed;
                            b.bodyRect.X += p.speed - b.speed;
                        }
                        else
                        {
                            b.bX += p.speed + b.speed;
                            b.bodyRect.X += p.speed + b.speed;
                        }
                    }
                }
                if (rechts == true)
                {
                    l.moveObj -= p.speed;
                    l.moveBack += 0.8;
                    foreach (Bamboo b in l.enemyCollection)
                    {
                        if (b.left == true)
                        {
                            b.bX -= p.speed+b.speed;
                            b.bodyRect.X -= p.speed + b.speed;
                        }
                        else
                        {
                            b.bX -= p.speed - b.speed;
                            b.bodyRect.X -= p.speed - b.speed;
                        }
                    }
                }
            }
            if (p.hitWallLeft)
            {
                if (p.lastDir == false)
                {
                    p.hitWallLeft = false;
                }
            }
            if (p.hitWallRight)
            {
                if (p.lastDir == true)
                {
                    p.hitWallRight = false;
                }
            }
        }//einde motion

        public void GameOver()
        {
            
        }
    }//einde manager class
}
