﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FysischeBib;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;

namespace ChildrenOfPanda
{
    public class Girlfriend : Pickup
    {
        private int tRect;
        private Surface mVideo;
        private Rectangle lRect;
        private int slower;

        public Girlfriend(Surface v, int x, int y)
            : base(v, x, y)
        {
            X = x;
            Y = y;
            tRect = 0;
            mVideo = v;

            oImage = new Surface("Guurlfriend.png");
            oRect = new Rectangle(X, Y, 126, 149);
            oImage.Transparent = true;
            oImage.TransparentColor = Color.Cyan;

        }

        public override void Update(TickEventArgs args)
        {
            slower++;
            if (slower >= 2)
            {
                tRect += 126;
                if (tRect >= 756)
                {
                    tRect = 0;
                }
                slower = 0;
            }
            oImage.Transparent = true;
            oImage.TransparentColor = Color.Cyan;
        }

        public override void Draw()
        {
            lRect = new Rectangle(tRect, 0, 126, 149);
            oRect.X = X + moveObj;
            mPosition = new Point(oRect.X, oRect.Y);
            oImage.Transparent = true;
            oImage.TransparentColor = Color.Cyan;
            mVideo.Blit(oImage, mPosition,lRect);
            Console.WriteLine(moveObj);
            Console.WriteLine(oRect.X);
            Console.WriteLine(lRect);
            Console.WriteLine(Y);
            Console.WriteLine(oY);
        }
    }
}

            
