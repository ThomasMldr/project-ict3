﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;

namespace ChildrenOfPanda
{
    public class Platform : BGObject
    {
        
        public Platform(Surface v, int x, int y)
            : base(v, x, y)
        {
            BGImage = new Surface("WoodPlatform.png").CreateScaledSurface(0.27);
            colRect = new Rectangle(x, y, 101, 3);
            
            
        }
    }
}
